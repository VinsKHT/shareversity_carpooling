CREATE TABLE Utilisateur(
    username VARCHAR(50) CONSTRAINT pk_Utilisateur PRIMARY KEY,
    type INT,
    nom_usr VARCHAR(50),
    mail_usr VARCHAR(50),
    prenom_usr VARCHAR(50),
    credit INT,
    password VARCHAR(50)
);

CREATE TABLE Commune(
    nom VARCHAR(50),
    code_postal INT,
    CONSTRAINT pk_Commune PRIMARY KEY(nom,code_postal)
);

CREATE TABLE Sponsor(
    nom_sponsor VARCHAR(50) CONSTRAINT pk_Sponsor PRIMARY KEY,
    mail_sponsor VARCHAR(50),
    adresse_sponsor VARCHAR(100)
);

CREATE TABLE Bon_achat(
    id_bon INT CONSTRAINT pk_Bon_achat PRIMARY KEY,
    nom_bon VARCHAR(50),
    description VARCHAR(50),
    valeur INT,
    nombre_disponible INT,
    nom_sponsor VARCHAR(50) NOT NULL,
    CONSTRAINT fk_Bon_achat_nom_sponsor FOREIGN KEY(nom_sponsor) REFERENCES Sponsor(nom_sponsor)
);

CREATE TABLE acheter(
    username VARCHAR(50),
    id_bon INT,
    date timestamp,
    CONSTRAINT pk_acheter PRIMARY KEY(username, id_bon,DATE),
    CONSTRAINT fk_acheter_username FOREIGN KEY(username) REFERENCES Utilisateur(username),
    CONSTRAINT fk_acheter_id_bon FOREIGN KEY(id_bon) REFERENCES Bon_achat(id_bon)
);

CREATE TABLE Aire_Covoiturage(
    
    latitude double precision,
    longitude double precision,
    nom_aire VARCHAR(50),
    nom_commune VARCHAR(50) NOT NULL,
    code_postal_commune INT NOT NULL,
    CONSTRAINT pk_Aire_Covoiturage PRIMARY KEY(latitude , longitude),
    CONSTRAINT fk_Aire_Covoiturage_Contenir FOREIGN KEY (nom, code_postal) REFERENCES Commune (nom_commune, code_postal_commune)
);

CREATE TABLE Trajet(
    id_trajet INT CONSTRAINT pk_Trajet PRIMARY KEY,
    duree INT,
    heure_depart TIME,
    distance INT,
    nombre_place INT,
    date DATE,
    status INT,
    username_proposer VARCHAR(50) NOT NULL,
    latitude_part double precision NOT NULL,
    longitude_part double precision NOT NULL,
    latitude_arriver double precision NOT NULL,
    longitude_arriver double precision NOT NULL,
    CONSTRAINT fk_Trajet_Aire_Covoiturage_part FOREIGN KEY (latitude_part,longitude_part) REFERENCES Aire_Covoiturage(latitude,longitude),
    CONSTRAINT fk_Trajet_Aire_Covoiturage_arriver FOREIGN KEY (latitude_arriver,longitude_arriver) REFERENCES Aire_Covoiturage(latitude,longitude),
    CONSTRAINT fk_Trajet_username_proposer FOREIGN KEY (username_proposer) REFERENCES Utilisateur(username)

);

CREATE TABLE note(
    id_note INT CONSTRAINT pk_note PRIMARY KEY,
    valeur INT,
    date DATE,
    username_posseder VARCHAR(50) NOT NULL,
    username_attribuer VARCHAR(50) NOT NULL,
    id_trajet_associer INT NOT NULL,
    CONSTRAINT fk_note_username_posseder FOREIGN KEY(username_posseder) REFERENCES Utilisateur(username),
    CONSTRAINT fk_note_username_attribuer FOREIGN KEY(username_attribuer) REFERENCES Utilisateur(username),
    CONSTRAINT fk_note_id_trajet_associer FOREIGN KEY(id_trajet_associer) REFERENCES Trajet(id_trajet)
);

CREATE TABLE reserver(
    username VARCHAR(50),
    id_trajet INT,
    CONSTRAINT pk_reserver PRIMARY KEY(username, id_trajet),
    CONSTRAINT fk_reserver_username FOREIGN KEY(username) REFERENCES Utilisateur(username),
    CONSTRAINT fk_reserver_id_trajet FOREIGN KEY(id_trajet) REFERENCES Trajet(id_trajet)
);

---insert users---
INSERT INTO utilisateur VALUES('AA',1,'Aoun','aa@irit.fr','Andre',100,'AA');
INSERT INTO utilisateur VALUES('MM',1,'Mojahid','MM@irit.fr','Mustapha',100,'MM');
INSERT INTO utilisateur VALUES('AV',0,'Vancel','AV@univ-tsle3.fr','Anthony',4,'AV');
INSERT INTO utilisateur VALUES('TV',0,'Vivier','TV@univ-tsle3.fr','Tristan',10,'TV');
INSERT INTO utilisateur VALUES('EH',0,'Hounzangbe','EH@univ-tsle3.fr','Emmanuel',50,'EH');
INSERT INTO utilisateur VALUES('KB',0,'boutillier','KB@univ-tsle3.fr','Kevin',500,'KB');

---inset commune---
INSERT INTO commune VALUES('ST PAUL les dax',40279);
INSERT INTO commune VALUES('Toulouse',31000);
INSERT INTO commune VALUES('Biganos',33051);
INSERT INTO commune VALUES('cestas',33122);
INSERT INTO commune VALUES('merignac',33281);


---insert sponsor---
INSERT INTO sponsor VALUES('total','sponsor@total.fr','Tour Coupole La Défense, 2 Pl. Jean Millier, 92078 Paris');
INSERT INTO sponsor VALUES('michelin','contactFr@michelin.com,23', 'place des Carmes-Dechaux 63040 CEDEX 9 CLERMONT-FERRAND, France');
INSERT INTO sponsor VALUES('norauto','com@norauto.fr','511-589 Rue des Seringats, 59262 Sainghin-en-Mélantois');
INSERT INTO sponsor VALUES('vinci','autoroute@vinci.fr','1 cours Ferdinand de Lesseps F-92851 Rueil-Malmaison Cedex - France');
INSERT INTO sponsor VALUES('pointS','contact@points.fr','9 RUE CURIE, 69006 LYON');
INSERT INTO sponsor VALUES('carglass','2euro@carglass.fr','107 Boulevard de la Mission Marchand 92411 Courbevoie');


---insert air---
INSERT INTO aire_covoiturage VALUES(1.37293,43.600469,'Gare Saint Martin du Touch','Toulouse',31000);
INSERT INTO aire_covoiturage VALUES(1.3665281,43.6074028,'Airbus M30','Toulouse',31000);
INSERT INTO aire_covoiturage VALUES(1.367199,43.6080364,'Airbus SaintMartin 2','Toulouse',31000);
INSERT INTO aire_covoiturage VALUES(1.3695751,43.6095368,'Airbus SaintMartin 1','Toulouse',31000);
INSERT INTO aire_covoiturage VALUES(1.3712541,43.6112054,'La Crabe - Airbus','Toulouse',31000);
INSERT INTO aire_covoiturage VALUES(1.428707,43.6139251,'Airbus St Eloi','Toulouse',31000);
INSERT INTO aire_covoiturage VALUES(1.414504,43.663942,'Lacourtensourt Gare SNCF','Toulouse',31000);

INSERT INTO aire_covoiturage VALUES(-0.973451,44.6396,'Parking Espace Culturel Lucien Mounaix','Biganos',33051);
INSERT INTO aire_covoiturage VALUES(-0.966781,44.636593,'Parking Rue des Papetiers','Biganos',33051);

INSERT INTO aire_covoiturage VALUES(-0.68727,44.758128,'Aire de Covoitrage de Cantelande','cestas',33122);
INSERT INTO aire_covoiturage VALUES(-0.737276,44.717383,'Aire de covoiturage de Jarry','cestas',33122);
INSERT INTO aire_covoiturage VALUES(-0.659193,44.736083,'Aire de covoiturage de Choisy','cestas',33122);


INSERT INTO aire_covoiturage VALUES(-0.654843,44.84572,'Parking Avenue du Maréchal de Lattre de Tassigny','merignac',33281);
INSERT INTO aire_covoiturage VALUES(-0.679277,44.830379,'Parking Décathlon Oxylane','merignac',33281);

INSERT INTO aire_covoiturage VALUES(-1.056509,43.745171,'Aire RD 947','ST PAUL les dax',40279);


---insert trajet---
INSERT INTO trajet VALUES(0,10,'15:00:00',2,5,'2021-01-05','AA',1.37293,43.600469,1.3665281,43.6074028,0);
INSERT INTO trajet VALUES(1,20,'12:00:00',14,3,'2021-02-10','MM',1.37293,43.600469,1.414504,43.663942,0);
INSERT INTO trajet VALUES(2,150,'15:00:00',250,5,'2021-02-25','TV',1.3665281,43.6074028,-0.679277,44.830379,0);
INSERT INTO trajet VALUES(3,150,'08:00:00',243,2,'2021-07-25','EH',-0.659193,44.736083,1.37293,43.600469,1);
INSERT INTO trajet VALUES(4,180,'18:00:00',224,5,'2021-07-25','KB',-1.056509,43.745171,1.367199,43.6080364,1);


---insert note---
INSERT INTO note VALUES(0,5,'2021-01-06','AA','MM',0);
INSERT INTO note VALUES(1,5,'2021-02-10','MM','AA',1);
INSERT INTO note VALUES(2,4,'2021-02-25','TV','EH',2);
INSERT INTO note VALUES(3,2,'2021-02-25','TV','KB',2);
commit;


---insert bon---
INSERT INTO bon_achat VALUES(0,'15$ carburant','15$ de carburant chez Total',20,50,'total');
INSERT INTO bon_achat VALUES(1,'30$ carburant','30$ de carburant chez Total',35,100,'total');
INSERT INTO bon_achat VALUES(2,'1 mois Vinci autoroute','1 mois d abonnement Vinci autoroute',5,100,'vinci');
INSERT INTO bon_achat VALUES(3,'6 mois Vinci autoroute','6 mois d abonnement Vinci autoroute',25,50,'vinci');
INSERT INTO bon_achat VALUES(4,'révision véhicule ','Une révision véhicule chez norauto',120,20,'norauto');
INSERT INTO bon_achat VALUES(5,'10% de réduction pneu','15% de réduction sur un train de pneu Michelin',40,10,'michelin');
INSERT INTO bon_achat VALUES(6,'paire essui glasses','Une paire d essui glasses Bosch chez carglass',60,200,'carglass');
INSERT INTO bon_achat VALUES(7,'1 lavage extérieur véhicule','1 bon pour lavage extérieur chez TOTAL WASH',5,150,'total');
INSERT INTO bon_achat VALUES(8,'1 lavage intérieur véhicule','1 bon pour lavage intérieur réalisé par un professionnel chez TOTAL WASH',35,50,'total');

commit;


---insert acheter---
INSERT INTO acheter VALUES('AV',0,'2021-01-25');
INSERT INTO acheter VALUES('TV',8,'2021-01-01');
INSERT INTO acheter VALUES('KB',2,'2021-02-05');
INSERT INTO acheter VALUES('AV',0,'2021-03-10');
INSERT INTO acheter VALUES('AV',4,'2021-02-18');
INSERT INTO acheter VALUES('EH',6,'2021-01-14');
commit;

---insert reserver---

INSERT INTO reserver VALUES('AA',1);
INSERT INTO reserver VALUES('MM',0);
INSERT INTO reserver VALUES('EH',2);
INSERT INTO reserver VALUES('KB',2);
INSERT INTO reserver VALUES('AA',3);
INSERT INTO reserver VALUES('AV',3);
INSERT INTO reserver VALUES('KB',3);
INSERT INTO reserver VALUES('AA',4);
INSERT INTO reserver VALUES('TV',4);




