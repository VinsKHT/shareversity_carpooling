<html>
<head>
    <title>Carte OpenStreetMap avec marqueur</title>
	<!-- http://wiki.openstreetmap.org/wiki/FR:OpenLayers_Simple_Example -->
   <style>
#cadrecarte{
   border-top : 1px solid #c0c0c0;
   border-bottom : 1px solid #c0c0c0;
   border-left : 1px solid #c0c0c0;
   border-right : 1px solid #c0c0c0;
   height:525px;
   width:567px;
   /*margin-left: 15px;*/
   }
 
 #carte {
   text-align : left;
   height:525px;
   width:567px;
   }
   
/****************Réduit le Copyright OSM en bas à droite**************/ 
	div.olControlAttribution, div.olControlScaleLine {
          font-family: Verdana;
          font-size: 0em;
          bottom: 3px;
        }
/*********************************************************************/    
   
</style>
</head>
	
<body>


<table style="margin-left: auto; margin-right:auto;">
<tbody>
  <tr>
   
   <td><div id="cadrecarte">
       <div id="carte"></div></div>
    </td>
	
  </tr>
</tbody>  
</table>

<!--------------------------------------------------------------------------------->

  <!--<div id="carte"></div>-->
  <script src="http://www.openlayers.org/api/OpenLayers.js"></script>
  <script>
    map = new OpenLayers.Map("carte");
    map.addLayer(new OpenLayers.Layer.OSM());
 
    var lonLat = new OpenLayers.LonLat(
    <?php 
    if(empty($_POST['GPS_arrivee'])){
        $donnees=explode(',',$_POST['GPS_depart']);
    }
    else{
        $donnees=explode(',',$_POST['GPS_arrivee']);
    }
    echo $donnees[0].",".$donnees[1];?>)
          .transform(
            new OpenLayers.Projection("EPSG:4326"), // transform from WGS 1984
            map.getProjectionObject() // to Spherical Mercator Projection
          );
 
    var zoom=12;
 
    var markers = new OpenLayers.Layer.Markers( "Markers" );
    map.addLayer(markers);
 
    markers.addMarker(new OpenLayers.Marker(lonLat));
 
    map.setCenter (lonLat, zoom);
  </script>
  
  
</body>
</html>