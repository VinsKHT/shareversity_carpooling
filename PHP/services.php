<?php

/**
 * vérifie si un utilisateur est connecté, sinon l'internaute est renvoyé vers la page de connexion est_connecte
 */
function est_connecte()
{
	session_start();
	if (!isset($_SESSION['id'])) {
		header("Location:../index.php");
	}
}

/**
 * @return bool donne vrai si quelqu'un est connecté, faux sinon
 */
function is_connected()
{
	session_start();
	if (isset($_SESSION['id'])) {
		return true;
	} else {
		return false;
	}
}

/** 
 *  récupère les trajets en cours et effectués par un utilisateur 
 * @return array<trajets> les trajets
 */
function recuperer_trajets()
{
	$id = $_SESSION['id'];
	$connect = pg_connect("host=otomny.fr port=5434 dbname=g2e user=g2e password=mWYmNBrxRI");
	$query = "	select distinct trajet.* 
				from trajet,reserver 
				where trajet.id_trajet in (
					(select distinct trajet.id_trajet from trajet,reserver where trajet.id_trajet=reserver.id_trajet and reserver.username='" . $id . "')
					UNION
					(select distinct trajet.id_trajet from trajet where trajet.username_proposer='" . $id . "'))
				order by status, date;";

	$ret = pg_query($connect, $query);
	if (!$ret) {
		exit;
	}
	pg_close($connect);
	return $ret;
}

/** 
 * récupère la ville en fonction de la latitude et de la longitude 
 * @param int $latitude
 * @param int $longitude
 * @return string le nom de la ville
 */
function recuperer_ville($latitude, $longitude)
{
	$connect = pg_connect("host=otomny.fr port=5434 dbname=g2e user=g2e password=mWYmNBrxRI");
	$query = "select commune.nom from commune, aire_covoiturage where aire_covoiturage.code_postal=commune.code_postal and latitude='" . $latitude . "' and longitude='" . $longitude . "'";
	$ret = pg_query($connect, $query);
	pg_close($connect);
	$row = pg_fetch_row($ret);
	return $row[0];
}

/** 
 * récupère le nom complet d'un utilisateur à partir de son id \
 * @param string $id
 * @return string nom et prenom du user
 */
function recuperer_nom_prenom_utilisateur($id)
{
	$connect = pg_connect("host=otomny.fr port=5434 dbname=g2e user=g2e password=mWYmNBrxRI");
	$query = "select prenom_usr, nom_usr from utilisateur where username='" . $id . "'";
	$ret = pg_query($connect, $query);
	pg_close($connect);
	$row = pg_fetch_row($ret);
	return $row[0] . " " . $row[1];
}

/**
 * Optimizing function of getting things and lowering response time (@by_manu)
 * @param string $id
 * @return string[] prenom_usr, nom_usr, mail_usr, credit
 */
function getUser($id)
{
	$connect = pg_connect("host=otomny.fr port=5434 dbname=g2e user=g2e password=mWYmNBrxRI");
	$query = "SELECT prenom_usr, nom_usr, mail_usr, credit from utilisateur WHERE username='" . $id . "'";
	$ret = pg_query($connect, $query);
	$row = pg_fetch_assoc($ret);

	return ($row);
}

/**
 * Récupérer le username de l'utilisateur actif 
 * @param string $id
 * @return string username
 */
function getUsername($id)
{
	$connect = pg_connect("host=otomny.fr port=5434 dbname=g2e user=g2e password=mWYmNBrxRI");
	$query = "select username from utilisateur where username='" . $id . "'";
	$ret = pg_query($connect, $query);
	pg_close($connect);
	$row = pg_fetch_row($ret);

	return $row[0];
}

/**
 * Récupérer le prénom de l'utilisateur actif 
 * @param string $id
 * @return string prenom_usr
 */
function getPrenom($id)
{
	$connect = pg_connect("host=otomny.fr port=5434 dbname=g2e user=g2e password=mWYmNBrxRI");
	$query = "select prenom_usr from utilisateur where username='" . $id . "'";
	$ret = pg_query($connect, $query);
	pg_close($connect);
	$row = pg_fetch_row($ret);
	return $row[0];
}

/**
 * Récupérer le nom de l'utilisateur actif 
 * @param string $id
 * @return string nom_usr
 */
function getNom($id)
{
	$connect = pg_connect("host=otomny.fr port=5434 dbname=g2e user=g2e password=mWYmNBrxRI");
	$query = "select nom_usr from utilisateur where username='" . $id . "'";
	$ret = pg_query($connect, $query);
	pg_close($connect);
	$row = pg_fetch_row($ret);
	return $row[0];
}

/**
 * Récupérer le mail de l'utilisateur actif 
 * @param string $id
 * @return string mail_usr
 */ 
function getMail($id)
{
	$connect = pg_connect("host=otomny.fr port=5434 dbname=g2e user=g2e password=mWYmNBrxRI");
	$query = "select mail_usr from utilisateur where username='" . $id . "'";
	$ret = pg_query($connect, $query);
	pg_close($connect);
	$row = pg_fetch_row($ret);
	return $row[0];
}

/**
 * Récupérer les credits de l'utilisateur actif 
 * @param string $id
 * @return string credit
 */
function getCredit($id)
{
	$connect = pg_connect("host=otomny.fr port=5434 dbname=g2e user=g2e password=mWYmNBrxRI");
	$query = "select credit from utilisateur where username='" . $id . "'";
	$ret = pg_query($connect, $query);
	pg_close($connect);
	$row = pg_fetch_row($ret);
	return $row[0];
}

/**
 * Récupérer le prix d'un bon d'achat
 * @param string $id
 * @return string valeur
 */
function getValeur($id)
{
	$connect = pg_connect("host=otomny.fr port=5434 dbname=g2e user=g2e password=mWYmNBrxRI");
	$query = "select valeur from bon_achat where id_bon='" . $id . "'";
	$ret = pg_query($connect, $query);
	pg_close($connect);
	$row = pg_fetch_row($ret);
	return $row[0];
}


/**
 * Récupérer le nombre de bon d'achat dispo
 * @param string $id
 * @return string nombre_disponible
 */
function getDispo($id)
{
	$connect = pg_connect("host=otomny.fr port=5434 dbname=g2e user=g2e password=mWYmNBrxRI");
	$query = "select nombre_disponible from bon_achat where id_bon='" . $id . "'";
	$ret = pg_query($connect, $query);
	pg_close($connect);
	$row = pg_fetch_row($ret);
	return $row[0];
}


/**
 * Récupérer les bon du l'utilisateur
 * @param string $id
 * @return string[] bon_achat.nom_bon,bon_achat.description,bon_achat.nom_sponsor,acheter.date
 */
function getBon($id)
{
	$connect = pg_connect("host=otomny.fr port=5434 dbname=g2e user=g2e password=mWYmNBrxRI");
	$query = "select bon_achat.nom_bon,bon_achat.description,bon_achat.nom_sponsor,acheter.date from acheter,bon_achat where acheter.id_bon=bon_achat.id_bon and acheter.username='" . $id . "'";
	$ret = pg_query($connect, $query);
	pg_close($connect);
	return $ret;
}

/**
 * Récupérer tout les bon d'achat
 * @param String $id
 * @return string[] bon_achat.nom_bon,bon_achat.description,bon_achat.nom_sponsor,bon_achat.valeur,bon_achat.id_bon
 */
function getallbon()
{
	$connect = pg_connect("host=otomny.fr port=5434 dbname=g2e user=g2e password=mWYmNBrxRI");
	$query = "select bon_achat.nom_bon,bon_achat.description,bon_achat.nom_sponsor,bon_achat.valeur,bon_achat.id_bon from bon_achat ";
	$ret = pg_query($connect, $query);
	pg_close($connect);
	return $ret;
}

/** 
 * initialise le tableau pour afficher les informations d'un bon
 * @param bool $bool_display
 */
function initialise_infos_bon($bool_display)
{
	echo "<thead>
					<th>Nom Bon</th>
					<th>description</th>
					<th>Sponsor</th>";
	if ($bool_display) {
		echo "		<th>Prix</th>";
		echo "		<th></th>";
	}

	echo 	"</thead>";
}
/** 
 * affiche les informations d'un bon
 * @param bool $bool_display
 */
function infos_bon($row, $bool_display)
{
	echo "<tr>
					<td>" . $row[0] . "</td>
					<td>" . $row[1] . "</td>
					<td>" . $row[2] . "</td>";
	if ($bool_display) {
		if (is_connected()) {
			echo "<td>" . $row[3] . "</td>";
			echo "<td><form action=\"/HTML/achat_bon.php\" method=\"post\" autocomplete=\"on\"><input class=\"button vert_back text_bold\" type=\"submit\" Value=\"Acheter\" name=\"" . $row[4] . "\"></form></td>";
		}
	}
	echo "</tr>";
}

/**
 * affiche les bon acheter
 * @param array $bon
 */
function afficher_mes_bon($bon)
{
	echo '<table class="table">';
	initialise_infos_bon(0);
	while ($row = pg_fetch_row($bon)) {
		infos_bon($row, 0);
	}
	echo "</table>";
}
/**
 * affiche les bon achetable
 * @param array $bon
 */
function afficher_bon_achat($bon)
{
	echo '<table class="table">';
	initialise_infos_bon(1);
	while ($row = pg_fetch_row($bon)) {
		infos_bon($row, 1);
	}
	echo "</table>";
}

/**
 * acheter les bon d'achat ssi on a les credits
 * @param String $id_bon
 * @param String $id_user
 */
function acheter_bon($id_bon,$id_user){
	$credit = getCredit($id_user);
	$valeur = getValeur($id_bon);
	$dispo = getDispo($id_bon);


	if ($credit >= $valeur){
		if ($dispo>0){
			$connect = pg_connect("host=otomny.fr port=5434 dbname=g2e user=g2e password=mWYmNBrxRI");
			//on ajoute l'achat dans la table achat
			$query = "insert into acheter VALUES('" . $id_user . "','". $id_bon ."','" . date("Y-m-d H:i:s") . "');";
			pg_query($connect,$query);

			//on reduit le nombre de bon disponible
			$dispo--;
			$data_dispo = array("nombre_disponible" => $dispo);
			$condition_bon = array("id_bon" => $id_bon);
			pg_update($connect, "bon_achat", $data_dispo, $condition_bon);

			//on reduit les credit user;
			$credit-=$valeur;
			
			$data_credit = array("credit" => $credit);
			$condition_utilisateur = array("username" => $id_user);
			pg_update($connect, "utilisateur", $data_credit, $condition_utilisateur);
			
			echo "Achat effectué.";
		}
		else {
			echo "Ce bon d'achat n'est plus disponible.";
			
		}

		pg_close($connect);
	}
	else{
		echo "Crédits insuffisants pour acheter ce bon.";
	}
	

}


/** 
 * initialise le tableau pour afficher les informations d'un trajet, 
 * si un utilisateur est connecté il créé un formulaire pour pouvoir réserver par la suite
 * @param bool $bool_display
 */
function initialise_infos_trajet($bool_display)
{

	echo "
		<thead>
				<th>Nom conducteur</th>
				<th>Lieu de départ</th>
				<th>Lieu d'arrivée</th>
				<th>Date</th>
				<th>Heure de départ</th>
				<th>Nombre de places restantes</th>";
	if ($bool_display) echo "		<th>Réservation</th>";
	echo "	</thead>";;
}

/** 
 * affiche les informations d'un trajet de manière lisible pour un utilisateur 
 * @param array $row
 * @param bool $bool_display
 * @param bool $bool_reserve
 */
function infos_trajet($row, $bool_display, $bool_reserve)
{
	echo "<tr>
					<td>" . recuperer_nom_prenom_utilisateur($row[6]) . "</td>
					<td>" . recuperer_ville($row[7], $row[8]) . "</td>
					<td>" . recuperer_ville($row[9], $row[10]) . "</td>
					<td>" . $row[5] . "</td>
					<td>" . $row[2] . "</td>
					<td>" . $row[4] . "</td>";
	if ($bool_display) {
		if (is_connected()) {
			if ($bool_reserve) {
				echo "<td><form action=\"/HTML/reservation.php\" method=\"post\" autocomplete=\"on\"><input class=\"button vert_back text_bold\" type=\"submit\" Value=\"Réserver\" name=\"" . $row[0] . "\"></form></td>";
			} else {
				echo "<td><form action=\"/HTML/annuler_reservation.php\" method=\"post\" autocomplete=\"on\"><input class=\"button rouge_back text_bold\" type=\"submit\" Value=\"Annuler\" name=\"" . $row[0] . "\"></form></td>";
			}
		} else {
			echo "<td><form method=\"get\" action=\"/HTML/connexion.html\"><input class=\"button is-warning\" type=\"submit\" Value=\"Se connecter\"></form></td>";
		}
	}
	echo "</tr>";
}

/**
 * affiche les trajets réalisés ou à venir
 * @param $trajet
 */
function afficher_trajets($trajets)
{
	$nb_trajets = pg_num_rows($trajets);

	if ($nb_trajets == 0) {
		echo "Vous n'avez pas encore réservé ou effectué de trajet.";
	} else {
		$row = pg_fetch_row($trajets);
		if ($row[11] == 0) {
			echo "<h2>Trajet(s) passé(s)</h2><br>";
			echo '<div class="box"><table class="table">';
			initialise_infos_trajet(0);
			infos_trajet($row, 0, 0);

			while ($row[11] == 0) {
				$row = pg_fetch_row($trajets);
				if ($row[11] == 0) {
					infos_trajet($row, 0, 0);
				}
			}
			echo "</table></div><br><br>";
		}
		if ($row[11] == 1) {
			echo "<h2>Trajet(s) à venir</h2><br>";
			echo '<div class="box"><table class="table">';
			initialise_infos_trajet(1);
			infos_trajet($row, 1, 0);
			while ($row = pg_fetch_row($trajets)) {
				infos_trajet($row, 1, 0);
			}
			if (is_connected()) {
				echo "</form>";
			}
			echo "</table></div>";
		}
	}
}


/** 
 * récupère les id des trajets partant de la ville donnée en paramètre et pour un jour donné 
 * @param string $ville
 * @param string $date
 * @return array id de villes
 */
function trouver_depart_ville($ville, $date)
{
	$retour = array();
	$connect = pg_connect("host=otomny.fr port=5434 dbname=g2e user=g2e password=mWYmNBrxRI");
	if (empty($date)) {
		$query = "select id_trajet from trajet, aire_covoiturage, commune where commune.code_postal=aire_covoiturage.code_postal and longitude_part=longitude and latitude_part=latitude and status = 1 and upper(commune.nom)='" . strtoupper($ville) . "'";
	} else {
		$query = "select id_trajet from trajet, aire_covoiturage, commune where commune.code_postal=aire_covoiturage.code_postal and longitude_part=longitude and latitude_part=latitude and trajet.date='" . $date . "' and status = 1 and upper(commune.nom)='" . strtoupper($ville) . "'";
	}

	$ret = pg_query($connect, $query);
	while ($row = pg_fetch_array($ret)) {
		array_push($retour, $row[0]);
	}
	pg_close($connect);
	return $retour;
}

/** 
 * idem mais pour une arrivée dans cette ville ce jour-là 
 * @param string $ville
 * @param string $date
 * @return array id de villes
 */
function trouver_arrivee_ville($ville, $date)
{
	$retour = array();
	$connect = pg_connect("host=otomny.fr port=5434 dbname=g2e user=g2e password=mWYmNBrxRI");
	if (empty($date)) {
		$query = "select id_trajet from trajet, aire_covoiturage, commune where commune.code_postal=aire_covoiturage.code_postal and longitude_arriver=longitude and latitude_arriver=latitude and status = 1 and upper(commune.nom)='" . strtoupper($ville) . "'";
	} else {
		$query = "select id_trajet from trajet, aire_covoiturage, commune where commune.code_postal=aire_covoiturage.code_postal and longitude_arriver=longitude and latitude_arriver=latitude and trajet.date='" . $date . "' and upper(commune.nom)='" . strtoupper($ville) . "'";
	}

	$ret = pg_query($connect, $query);
	while ($row = pg_fetch_array($ret)) {
		array_push($retour, $row[0]);
	}
	pg_close($connect);
	return $retour;
}

/** 
 * retourne les trajets en commun entre deux villes le même jour 
 * @param array $depart
 * @param array $arrivee
 * @return array id trajet en commun
 */
function trajets_commun($depart, $arrivee)
{
	$ret = array();
	for ($i = 0; $i < count($depart); $i++) {
		for ($j = 0; $j < count($arrivee); $j++) {
			if ($depart[$i] == $arrivee[$j]) {
				array_push($ret, $depart[$i]);
			}
		}
	}
	return $ret;
}

/** 
 * retourne les informations complètes d'un trajet d'après son id 
 * @param int $id
 * @return string[] * from trajet
 */
function infos_trajet_brut($id)
{
	$connect = pg_connect("host=otomny.fr port=5434 dbname=g2e user=g2e password=mWYmNBrxRI");
	$query = "select * from trajet where id_trajet='" . $id . "'";
	$ret = pg_query($connect, $query);
	pg_close($connect);
	return pg_fetch_row($ret);
}
 
/**
 * affiche les résultats d'une recherche 
 * @param array $depart
 * @param array $arrivee
 * @param string $date
 */
function afficher_resultats_recherche($depart, $arrivee, $date)
{

	$id_departs = trouver_depart_ville($depart, $date);
	$id_arrivees = trouver_arrivee_ville($arrivee, $date);
	$id_commun = trajets_commun($id_departs, $id_arrivees);

	if (count($id_commun) == 0) {
		echo "Désolé mais nous n'avons pas trouvé de trajet correspondant à votre demande.";
	} else {
		echo '<table class="table">';
		initialise_infos_trajet(1);
		for ($i = 0; $i < count($id_commun); $i++) {
			infos_trajet(infos_trajet_brut($id_commun[$i]), 1, 1);
		}

		echo "</table><br>";
	}
}

/**
 * renvoie le nombre de places d'un trajet
 * @param string $id_trajet
 * @return string nombre_place
 */
function getPlaces($id_trajet)
{
	$connect = pg_connect("host=otomny.fr port=5434 dbname=g2e user=g2e password=mWYmNBrxRI");
	$query = "select nombre_place from trajet where id_trajet='" . $id_trajet . "'";
	$ret = pg_query($connect, $query);
	pg_close($connect);
	$row = pg_fetch_row($ret);
	return $row[0];
}

/**
 * vérifie qu'un utilisateur a assez de crédit pour réserver un trajet, renvoie true dans ce cas, false sinon
 * @param string $id_user identifiant utilisateur
 * @return bool true si assez decredit false si non 
 */
function credits_suffisants($id_user)
{
	$connect = pg_connect("host=otomny.fr port=5434 dbname=g2e user=g2e password=mWYmNBrxRI");
	$query = "select credit from utilisateur WHERE username='" . $id_user . "'";
	$ret = pg_query($connect, $query);
	$row = pg_fetch_row($ret);
	pg_close($connect);
	if ($row[0] > 0) {
		return true;
	} else {
		return false;
	}
}

/** 
 * réserve un trajet pour l'utilisateur connecté sur un trajet choisi à l'avance et donne un crédit au conducteur
 * @param string $id_trajet
 * @param string $id_user
 */
function reserver_trajet($id_trajet, $id_user)
{
	$nombre_places = getPlaces($id_trajet);
	if ($nombre_places > 0) {
		if (credits_suffisants($id_user)) {

			/* on ajoute d'abord le passager */
			$connect = pg_connect("host=otomny.fr port=5434 dbname=g2e user=g2e password=mWYmNBrxRI");

			$query_ajouter_passager = "INSERT INTO reserver VALUES('" . $id_user . "'," . $id_trajet . ");";
			pg_query($connect, $query_ajouter_passager);
			pg_close($connect);

			/* on baisse maintenant le nombre de crédits du passager */
			$credits = getCredit($id_user);
			$credits--;
			$data_credits = array("credit" => $credits);
			$condition_credits = array("username" => $id_user);
			$connect = pg_connect("host=otomny.fr port=5434 dbname=g2e user=g2e password=mWYmNBrxRI");
			$ret2 = pg_update($connect, "utilisateur", $data_credits, $condition_credits);

			if (!$ret2) {
				echo "y'a eu un pb";
			}

			/* on baisse le nombre de places disponibles pour le trajet */
			$nombre_places--;
			$data_places = array("nombre_place" => $nombre_places);
			$condition_places = array("id_trajet" => $id_trajet);
			$ret3 = pg_update($connect, "trajet", $data_places, $condition_places);
			if (!$ret3) {
				echo "y'a eu un pb ici aussi";
			}
			pg_close($connect);

			/* et enfin on augmente le nombre de crédits du conducteur d'un point */
			augmenter_credits_conducteur($id_trajet);


			echo "Vous avez bien réservé votre trajet. Vous pouvez maintenant le voir dans vos trajets à venir.";
		} else {
			echo "Désolé mais vous n'avez plus de crédits et vous ne pouvez donc plus réserver de trajets.
			<br>
			Proposez un trajet ou attendez le mois prochain pour gagner des crédits.";
		}
	} else {
		echo "Désolé mais le trajet que vous avez choisi n'a plus aucune place disponible.";
	}
}

/**
 * fonction d'annulation de trajet mixte 
 * @param string $id_trajet
 * @param string $id_user
 */
function annuler_trajet($id_trajet, $id_user)
{
	$connect = pg_connect("host=otomny.fr port=5434 dbname=g2e user=g2e password=mWYmNBrxRI");

	$query = "select username_proposer from trajet where id_trajet='" . $id_trajet . "';";

	$ret = pg_query($connect, $query);
	pg_close($connect);
	$id_conducteur = pg_fetch_row($ret);

	echo $id_conducteur[0];
	if ($id_conducteur[0] != $id_user) {
		echo "annulation de reservation";
		annuler_trajet_reserver($id_trajet, $id_user);
	} else {
		echo "annulation de trajet";
		annuler_trajet_conducteur($id_trajet);
	}
}
/**
 * annulation si on est non conducteur
 * @param string $id_trajet
 * @param string $id_user
 */
function annuler_trajet_reserver($id_trajet, $id_user)
{
	//on supprime la reservation de la table reserver
	$connect = pg_connect("host=otomny.fr port=5434 dbname=g2e user=g2e password=mWYmNBrxRI");
	$trajet = array("id_trajet" => $id_trajet, "username" => $id_user);
	pg_delete($connect, 'reserver', $trajet);
	pg_close($connect);

	//on augmente les places dispo dans le trajet
	$nombre_places = getPlaces($id_trajet);

	$connect = pg_connect("host=otomny.fr port=5434 dbname=g2e user=g2e password=mWYmNBrxRI");
	$nombre_places++;
	$data_places = array("nombre_place" => $nombre_places);
	$condition_places = array("id_trajet" => $id_trajet);
	pg_update($connect, "trajet", $data_places, $condition_places);
	pg_close($connect);
}

/**
 * annulation si on est conducteur
 */
function annuler_trajet_conducteur($id_trajet)
{
	//on suprimme toute les reservation du trajet
	$connect = pg_connect("host=otomny.fr port=5434 dbname=g2e user=g2e password=mWYmNBrxRI");
	$trajet = array("id_trajet" => $id_trajet);
	pg_delete($connect, 'reserver', $trajet);
	pg_close($connect);
	//on envoie un mail a tout les reserver (impossible car localhost)

	//on change le status du trajet le trajet
	$connect = pg_connect("host=otomny.fr port=5434 dbname=g2e user=g2e password=mWYmNBrxRI");
	$trajet = array("id_trajet" => $id_trajet);
	$data = array("status" => 0);
	pg_update($connect, "trajet", $data, $trajet);
	pg_close($connect);
}

/**
 * récupère les aire de covoiturage d'une ville donnée avec latitude et longitude correspondantes
 * @param string $ville
 * @return array aires d'une ville
 */
function recuperer_aires($ville)
{
	$retour = array();
	$connect = pg_connect("host=otomny.fr port=5434 dbname=g2e user=g2e password=mWYmNBrxRI");
	$query = "select nom_aire, latitude, longitude from aire_covoiturage where upper(nom)='" . strtoupper($ville) . "'";
	$ret = pg_query($connect, $query);
	pg_close($connect);
	while ($row = pg_fetch_row($ret)) {
		array_push($retour, $row);
	}
	return $retour;
}

/** 
 * affiche les aires de covoiturage dans une liste déroulante
 * @param array $aires
 */
function afficher_liste_deroulante_aires($aires)
{
	for ($i = 0; $i < count($aires); $i++) {
		echo "<option value=\"" . $aires[$i][1] . "," . $aires[$i][2] . "\">" . $aires[$i][0] . "</option>";
	}
}


/**
 * calcule la distance entre deux coordonnées GPS
 * @param float $lat1
 * @param float $lng1
 * @param float $lat2
 * @param float $lng2
 * @return float distance entre 2 points
 */
function get_distance_m($lat1, $lng1, $lat2, $lng2)
{
	$earth_radius = 6372.795477598;  // Terre = sphère de 6378km de rayon
	$dla = ($lat1 - $lat2) / 2;
	$dlo = ($lng1 - $lng2) / 2;
	$rla1 = deg2rad($lat1);
	$rla2 = deg2rad($lat2);
	$alpha    = deg2rad($dla);
	$beta     = deg2rad($dlo);
	$a = (sin($alpha) * sin($alpha)) + cos($rla1) * cos($rla2) * (sin($beta) * sin($beta));
	$c = asin(min(1, sqrt($a)));
	$distance  = 2 * $earth_radius *$c;
	return $distance;
}

/**
 * créé un trajet et augmente les crédits de l'utilisateur qui propose le trajet
 * @param float $distance
 * @param string $date
 * @param string $depart
 * @param string $arrivee
 * @param int $nb_places
 * @param string $id_user
 * @param int $heure_depart
 */
function creer_trajet($distance, $date, $depart, $arrivee, $nb_places, $id_user, $heure_depart)
{
	$id_trajet = dernier_id_trajet();
	$id_trajet[0]++;
	$temps = (int)$distance * 60 / 100;
	$connect = pg_connect("host=otomny.fr port=5434 dbname=g2e user=g2e password=mWYmNBrxRI");
	$query_trajet = "INSERT INTO trajet VALUES(" . $id_trajet[0] . "," . $temps . ",'" . $heure_depart . "'," . $distance . "," . $nb_places . ",'" . $date . "','" . $id_user . "'," . $depart[0] . "," . $depart[1] . "," . $arrivee[0] . "," . $arrivee[1] . ",1)";
	$ret_trajet = pg_query($connect, $query_trajet);
	if (!empty($ret_trajet)) {
		echo "Votre trajet a bien été créé. Vous pouvez désormais le voir dans vos trajets.";
	} else {
		echo "Il y a eu une erreur lors de la création de votre trajet. Veuillez recommencer.";
	}
	pg_close($connect);
}

/**
 * récupère l'id le plus élevé d'un trajet pour push a la fin
 * @return int max id 
 */
function dernier_id_trajet()
{
	$connect = pg_connect("host=otomny.fr port=5434 dbname=g2e user=g2e password=mWYmNBrxRI");
	$query = "select max(id_trajet) from trajet";
	$ret = pg_query($connect, $query);
	pg_close($connect);
	return pg_fetch_row($ret);
}

/**
 * augmente d'un crédit le conducteur du trajet
 * @param int $id_trajet
 */
function augmenter_credits_conducteur($id_trajet){
	$connect = pg_connect("host=otomny.fr port=5434 dbname=g2e user=g2e password=mWYmNBrxRI");
	$query = "select username_proposer from trajet where id_trajet=".$id_trajet;
	$ret=pg_query($connect, $query);
	$id_conducteur=pg_fetch_row($ret)[0];
	pg_close($connect);

	$credits=getCredit($id_conducteur);
	$credits++;
	$data_credits = array("credit" => $credits);
	$condition_credits = array("username" => $id_conducteur);
	$connect = pg_connect("host=otomny.fr port=5434 dbname=g2e user=g2e password=mWYmNBrxRI");
	$ret2 = pg_update($connect, "utilisateur", $data_credits, $condition_credits);
	pg_close($connect);
}