<?php
    //On définit dans notre cas les identifiants en tant que constantes
    define('LOGIN','admin');
    define('PASSWORD','admin');
    $messagerErr = '';
    //On teste l'envoi du formulaire
    
    if(!empty($_POST["submit_connect"]))
    {   
        //vérification de transmission des identifiants
        if (!empty($_POST['login']) && !empty($_POST['password']))
        {
            
            //On vérifie que l'entrée des champs correspond aux valeurs définies
            if ($_POST['login'] !== LOGIN)
            {
                $messagerErr = 'Login incorrecte';
            }elseif($_POST['password'] !== PASSWORD)
            {
                $messagerErr = 'Mot de passe incorrecte';
            }
            
            else//cas standard ou la paire login mdp est correcte
            {
                //On démarre la session
                session_start();
                $_SESSION['login'] = LOGIN;
                $_SESSION['password'] = PASSWORD;
                //var_dump($_SESSION);
                //exit();
                header('Location: formation.php');
            }
           
        }
    }
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>DM3_Exercice2</title>
</head>
<body>
   <form action="connexion.php" method="post">
       <fieldset>
           <legend>Identification</legend>
           <?php
                if (isset($messagerErr))
                {
                    echo '<p>',htmlspecialchars($messagerErr),'</p>';
                }
           ?>
           <label for="log">Login:</label><br>
           <input type="text" name="login" id="login"><br>
           <label for="pwd">Mot de passe:</label><br>
           <input type="password" name="password" id="password"><br><br>
           <input type="submit" value="Soumettre" name="submit_connect">
       </fieldset>
   </form>
</body>
</html>

<?php
    if (isset($_GET["disconnected"]) && $_GET["disconnected"] == "1")
    {
        session_destroy();
        unset($_SESSION['login']);
        
    }
?>