<?php
    session_start();
    if(empty($_SESSION['login']))
    {
        header('Location : connexion.php');
        exit();
    }
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <a href="./connexion.php?disconnected=1">Déconnexion</a>
    <fieldset>
        <legend>Cours disponibles</legend>
        <ul>
            <li><a href="./cours1.php">Cour 1</a></li>
            <li><a href="./cours2.php">Cours 2</a></li>
        </ul>
    </fieldset>
</body>
</html>