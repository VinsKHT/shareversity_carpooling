<?php
session_start();
if(empty($_SESSION['login']))
{
    header('Location: connexion.php');
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cours 1</title>
    <style>
      ul {
  list-style-type: none;
  margin: 0;
  padding: 0;
}  
    </style>
</head>
<body>
    <h1>Cours 1</h1>

    <nav>
        <ul>
            <li><a href="./cours1.php">Cours 1</a></li>
            <li><a href="./cours2.php">Cours 2</a></li>
        </ul>
    </nav>
    <p>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec quis iaculis quam. Ut fringilla sapien tempor egestas egestas. Etiam tristique eget neque id faucibus. Maecenas turpis dolor, suscipit sit amet lacus in, maximus egestas dolor. Nunc tristique ornare quam, accumsan facilisis tortor porttitor in. Nam commodo risus eget nibh tempor, eget malesuada sapien posuere. Aenean in consequat sem. Aliquam molestie nunc vitae orci lacinia, quis molestie nisl fringilla. In sed nibh et sapien ullamcorper cursus quis sit amet nunc. Nullam porttitor, urna ac lacinia sollicitudin, arcu libero gravida lectus, eget suscipit libero turpis quis neque. Sed nec tellus sagittis, molestie augue et, vestibulum arcu. Praesent erat lacus, pretium nec semper id, tincidunt sed ipsum. Praesent sit amet nisi nec mi eleifend euismod. Proin lacinia sit amet tellus vel fermentum. Integer maximus viverra lorem, at ornare lorem ornare quis. Curabitur fermentum ut orci ut vehicula. 
    </p>
</body>
</html>