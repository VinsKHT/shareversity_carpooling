<!DOCTYPE html>
<html>
    <?php
    include '../PHP/services.php';
    est_connecte();
    $infos_usr = getUser($_SESSION['id']);
    ?>
    <!--verif sess auth-->

    <head>
        <title>MON COMPTE</title>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="../CSS/style.css" />
    </head>

    <body>
        <header>
            <nav class="navbar is-black" role="navigation" aria-label="main navigation">
                <div class="navbar-brand">
                    <a class="navbar-item" href="\index.php">
                        <img src="\IMAGES\logo_shareversity.png" >SHAREVERSITY
                    </a>

                    <a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
                        <span aria-hidden="true"></span>
                        <span aria-hidden="true"></span>
                        <span aria-hidden="true"></span>
                    </a>
                </div>

                <div id="nav-links" class="navbar-menu is-active">
                    <!--Toggle menu actif sur les devices de taille inférieure à 1024px-->
                    <div class="navbar-end">
                        <a class="navbar-item" href="\index.php">
                            Accueil
                        </a>

                        <a class="navbar-item" href="\HTML\trajets.php">
                            Mes Trajets
                        </a>

                        <div class="navbar-item has-dropdown is-hoverable">
                            <a class="navbar-link">
                                Plus
                            </a>

                            <div class="navbar-dropdown is-right">
                                <a class="navbar-item">
                                    À Propos
                                </a>

                                <a class="navbar-item">
                                    Contact
                                </a>
                                <hr class="navbar-divider">
                                <a class="navbar-item">
                                    Signaler un problème
                                </a>
                            </div>
                        </div>
                    </div>

                </div>
            </nav>
        </header>
        

        <section class="section">
            <div class="container is-widescreen">
                <h2 class="title is-3 has-text-centered">Mon compte</h2>

                <div class="columns is-mobile is-centered">
                    <div class="column is-9">
        
                        <div class="box">
                            <h4><strong>Ma bio</strong> </h4>
                            <p>Username : <?= $_SESSION['id'];?></p>
                            <p>NOM: <?= isset($infos_usr["nom_usr"]) ? $infos_usr["nom_usr"] : null ?></p>
                            <p>PRENOM: <?= isset($infos_usr["prenom_usr"]) ? $infos_usr["prenom_usr"] : null ?></p>
                            <p>E-MAIL: <?= isset($infos_usr["mail_usr"]) ? $infos_usr["mail_usr"] : null ?></p>
                            <p>CREDIT: <?= isset($infos_usr["credit"]) ? $infos_usr["credit"] : null ?></p>
                        </div>
                        <div class="box">
                            <h4><strong>Préférences de voyage</strong></h4>
                        </div>
                        <div class="box">
                            <h4><strong>Mes bons</strong></h4>
                            <?php afficher_bon(getBon($_SESSION['id'])) ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>


    </body>

    <footer class="footer has-text-centered">
        <p>Copyleft <span class="copyleft">&copy;</span> - VIVIER Tristan : HOUNZANGBE Emmanuel : VANCEL Anthony : BOUTILLIER Kevin</p>
    </footer>

</html>