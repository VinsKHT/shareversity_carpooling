<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Shareversity</title>
    <link rel="stylesheet" href="/CSS/style.css" />
    <script src="/JavaScript/index.js"></script>
</head>

<body>
    <!-- Nav -->
    <header>
        <nav class="navbar is-black" role="navigation" aria-label="main navigation">
            <!--Logo-->
            <div class="navbar-brand">
                <a class="navbar-item" href="\index.php">
                    <img class="image" src="\IMAGES\logo_shareversity_v2.png" style="max-height: 70px">
                </a>
                <!--Bouton pour la vue du menu sur smartphone-->
                <a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false" data-target="nav-links">
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                </a>
            </div>

            <div class="navbar-menu" id="nav-links">
                <!--Toggle menu actif sur les devices de taille inférieure à 1024px-->
                <div class="navbar-end">

                    <?php
                    if (isset($_SESSION['id'])) {
                        echo '<a class="navbar-item" href="/HTML/compte.php">Mon compte</a>';
                        echo '<a class="navbar-item" href="/HTML/trajets.php">Mes trajets</a>';
                        echo '<a class="navbar-item" href="/HTML/question.php">Questions</a>';
                    } else {
                        echo '<a class="navbar-item" href="/HTML/connexion.html">Connexion</a>';
                        echo '<a class="navbar-item" href="/HTML/creation.php">Création compte</a>';
                        echo '<a class="navbar-item" href="/HTML/question.php">Questions</a>';
                    }
                    ?>

                    <div class="navbar-item has-dropdown is-hoverable">
                        <a class="navbar-link">
                            Plus
                        </a>

                        <div class="navbar-dropdown is-right">
                            <a class="navbar-item">
                                À Propos
                            </a>

                            <a class="navbar-item" href="/HTML/aboutus.php">
                                Contact
                            </a>
                            <a class="navbar-item" href="/HTML/feedback.php">
                                Signaler un problème
                            </a>
                            <hr class="navbar-divider">
                            <?= isset($_SESSION['id']) ? '<a class="navbar-item" href="/PHP/deconnexion.php">Deconnexion compte</a>' : null ?>


                        </div>
                    </div>
                </div>

            </div>
        </nav>
    </header>

    <!-- Cover -->
    <section class="hero is-dark is-fullheight-with-navbar" style="background-image: url(/IMAGES/cover_image.jpg); background-size: cover; background-blend-mode: soft-light;">
        <div class="hero-body ">
            <div class="container is-widescreen">
                <h2 class="title is-3 has-text-centered">Recherche Trajet</h2>

                <div class="columns is-mobile is-centered">
                    <div class="column is-9">
                        <div class="box">
                            <div class="field">
                                <label class="label">Recherche de trajets</label>
                                <div class="control">
                                    <form action="/HTML/recherche_trajets.php" method="post" autocomplete="on">
                                        <div class="columns">
                                            <div class="column">
                                                <input class="input" type="text" name="depart" id="departure" placeholder="Départ">
                                            </div>
                                            <div class="column">
                                                <input class="input" type="text" name="arrivee" id="arrival" placeholder="Arrivée" />
                                            </div>
                                            <div class="column">
                                                <input class="input" type="date" name="date" id="date" />
                                            </div>
                                        </div>
                                        <div class="columns is-centered">
                                            <input class="button blue_back text_bold" type="submit" value="Rechercher" />
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Main -->
    <section class="section">
        <h1 class="title">Qui sommes-nous ?</h1>
        <h2 class="subtitle">
            lorem ipsum
        </h2>
        <div class="columns">
            <div class="column">
                <div class="card">
                    <div class="card-image">
                        <figure class="image is-4by3">
                            <img src="https://bulma.io/images/placeholders/1280x960.png" alt="Placeholder image">
                        </figure>
                    </div>
                    <div class="card-content">
                        <div class="media">
                            <div class="media-left">
                                <figure class="image is-48x48">
                                    <img src="https://bulma.io/images/placeholders/96x96.png" alt="Placeholder image">
                                </figure>
                            </div>
                            <div class="media-content">
                                <p class="title is-4">John Smith</p>
                                <p class="subtitle is-6">@johnsmith</p>
                            </div>
                        </div>

                        <div class="content">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Phasellus nec iaculis mauris. <a>@bulmaio</a>.
                            <a href="#">#css</a> <a href="#">#responsive</a>
                            <br>
                            <time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
                        </div>
                    </div>
                </div>
            </div>
            <div class="column">
                <div class="card">
                    <div class="card-image">
                        <figure class="image is-4by3">
                            <img src="https://bulma.io/images/placeholders/1280x960.png" alt="Placeholder image">
                        </figure>
                    </div>
                    <div class="card-content">
                        <div class="media">
                            <div class="media-left">
                                <figure class="image is-48x48">
                                    <img src="https://bulma.io/images/placeholders/96x96.png" alt="Placeholder image">
                                </figure>
                            </div>
                            <div class="media-content">
                                <p class="title is-4">John Smith</p>
                                <p class="subtitle is-6">@johnsmith</p>
                            </div>
                        </div>

                        <div class="content">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Phasellus nec iaculis mauris. <a>@bulmaio</a>.
                            <a href="#">#css</a> <a href="#">#responsive</a>
                            <br>
                            <time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
                        </div>
                    </div>
                </div>
            </div>
            <div class="column">
                <div class="card">
                    <div class="card-image">
                        <figure class="image is-4by3">
                            <img src="https://bulma.io/images/placeholders/1280x960.png" alt="Placeholder image">
                        </figure>
                    </div>
                    <div class="card-content">
                        <div class="media">
                            <div class="media-left">
                                <figure class="image is-48x48">
                                    <img src="https://bulma.io/images/placeholders/96x96.png" alt="Placeholder image">
                                </figure>
                            </div>
                            <div class="media-content">
                                <p class="title is-4">John Smith</p>
                                <p class="subtitle is-6">@johnsmith</p>
                            </div>
                        </div>

                        <div class="content">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Phasellus nec iaculis mauris. <a>@bulmaio</a>.
                            <a href="#">#css</a> <a href="#">#responsive</a>
                            <br>
                            <time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
                        </div>
                    </div>
                </div>
            </div>
            <div class="column">
                <div class="card">
                    <div class="card-image">
                        <figure class="image is-4by3">
                            <img src="https://bulma.io/images/placeholders/1280x960.png" alt="Placeholder image">
                        </figure>
                    </div>
                    <div class="card-content">
                        <div class="media">
                            <div class="media-left">
                                <figure class="image is-48x48">
                                    <img src="https://bulma.io/images/placeholders/96x96.png" alt="Placeholder image">
                                </figure>
                            </div>
                            <div class="media-content">
                                <p class="title is-4">John Smith</p>
                                <p class="subtitle is-6">@johnsmith</p>
                            </div>
                        </div>

                        <div class="content">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Phasellus nec iaculis mauris. <a>@bulmaio</a>.
                            <a href="#">#css</a> <a href="#">#responsive</a>
                            <br>
                            <time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</body>
<!-- Footer -->
<footer>
    <footer class="footer has-text-centered">
        <p>Copyleft <span class="copyleft">&copy;</span> - VIVIER Tristan : HOUNZANGBE Emmanuel : VANCEL Anthony : BOUTILLIER Kevin</p>
    </footer>
</footer>

</html>