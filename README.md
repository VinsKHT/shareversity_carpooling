# Shareversity - Covoiturage entre étudiants

*Plateforme de covoiturage participatif* 


## 📚 Auteurs

👤 **E.HOUNZANGBE**

👤 **A.VANCEL**

👤 **T.VIVIER**

👤 **K. BOUTILLER**


## 💻 Installation et dépendances

Ce projet utilise:
PHP, HTML & CSS




## 🚀 Utilisation



## 📝 License

Copyleft © 2020-2021 E.HOUNZANGBE, A.VANCEL, T.VIVIER, K.BOUTILLIER
This project is OpenSource.

<img src="https://stri-online.net/FTLV/pluginfile.php/1/theme_adaptable/adaptablemarketingimages/0/Logos.png" width="50%" alt="banner_upssitech"/>
