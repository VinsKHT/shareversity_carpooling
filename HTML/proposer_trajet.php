<!DOCTYPE html>
<html>
<?php
session_start();
?>
<!--verif sess auth-->

<head>
    <title>Shareversity</title>
    <meta charset="utf-8" />
    <link rel="stylesheet" href="/CSS/style.css" />
    <script src="/JavaScript/index.js"></script>
</head>

<body>
    <header>
        <nav class="navbar is-black" role="navigation" aria-label="main navigation">
            <!--Logo-->
            <div class="navbar-brand">
                <a class="navbar-item" href="\index.php">
                    <img class="image" src="\IMAGES\logo_shareversity_v2.png" style="max-height: 70px">
                </a>
                <!--Bouton pour la vue du menu sur smartphone-->
                <a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false" data-target="nav-links">
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                </a>
            </div>

            <div class="navbar-menu" id="nav-links">
                <!--Toggle menu actif sur les devices de taille inférieure à 1024px-->
                <div class="navbar-end">
                    <!-- affichage selectif sur session -->
                    <a class="navbar-item" href="/index.php">Accueil</a>
                    <a class="navbar-item" href="compte.php">Mon compte</a>
                    <a class="navbar-item" href="/HTML/trajets.php">Mes trajets</a>
                    <a class="navbar-item" href="/HTML/bon.php">Bon achat</a>
                    <a class="navbar-item" href="/HTML/question.php">Questions</a>
                    
                    

                    <div class="navbar-item has-dropdown is-hoverable">
                        <a class="navbar-link">
                            Plus
                        </a>

                        <div class="navbar-dropdown is-right">
                            <a class="navbar-item">
                                À Propos
                            </a>

                            <a class="navbar-item" href="/HTML/feedback.php">
                                Signaler un problème
                            </a>
                            <hr class="navbar-divider">
                            <!-- affichage selectif sur session -->
                            <?= isset($_SESSION['id']) ? '<a class="navbar-item" href="/PHP/deconnexion.php">Deconnexion compte</a>' : null ?>


                        </div>
                    </div>
                </div>

            </div>
        </nav>
    </header>
    <section class="hero is-dark is-fullheight-with-navbar" style="background-image: url(/IMAGES/cover_image.jpg); background-size: cover; background-blend-mode: soft-light;">
        <div class="hero-body ">
            <div class="container is-widescreen">
                <div class="columns is-mobile is-centered">
                    <div class="column is-9">                       
                        <h2 class="title is-3 has-text-centered">Proposer un trajet</h2>
                        <div class="box">
                            <div class="field">
                                <label class="label">Proposer un trajet</label>
                                <div class="control">
                                    <form action="/HTML/proposer.php" method="post" autocomplete="on">
                                        <div class="columns">
                                            <div class="column">
                                                <input class="input" type="text" name="depart" id="departure" placeholder="Départ">
                                            </div>
                                            <div class="column">
                                                <input class="input" type="text" name="arrivee" id="arrival" placeholder="Arrivée" />
                                            </div>
                                            <div class="column">
                                                <input class="input" type="date" name="date" id="date" />
                                            </div>
                                            <div class="column">
                                                <input class="input" type="text" name="nb_places" id="nb_places" placeholder="Nombre de places" S />
                                            </div>
                                            <div class="column">
                                                <input class="input" type="time" name="heure_depart" id="heure_depart" />
                                            </div>
                                        </div>
                                        <div class="columns is-centered">
                                            <input class="button blue_back text_bold" type="submit" value="Proposer" />
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</body>

<footer class="footer has-text-centered">
    <p>Copyleft <span class="copyleft">&copy;</span> - VIVIER Tristan : HOUNZANGBE Emmanuel : VANCEL Anthony : BOUTILLIER Kevin</p>
</footer>

</html>