<!DOCTYPE html>
<html>
<?php
include '../PHP/services.php';
est_connecte();
?>
<!--verif sess auth-->

<head>
    <title> Shareversity - Bon achat</title>
    <meta charset="utf-8" />
    <link rel="stylesheet" href="../CSS/style.css" />
    <script src="/JavaScript/index.js"></script>
</head>

<body>
    <header>
        <nav class="navbar is-black" role="navigation" aria-label="main navigation">
            <!--Logo-->
            <div class="navbar-brand">
                <a class="navbar-item" href="\index.php">
                    <img class="image" src="\IMAGES\logo_shareversity_v2.png" style="max-height: 70px">
                </a>
                <!--Bouton pour la vue du menu sur smartphone-->
                <a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false" data-target="nav-links">
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                </a>
            </div>

            <div id="nav-links" class="navbar-menu">
                <!--Toggle menu actif sur les devices de taille inférieure à 1024px-->
                <div class="navbar-end">
                    <a class="navbar-item" href="/index.php">Accueil</a>
                    <a class="navbar-item" href="/HTML/compte.php">Mon compte</a>   
                    <a class="navbar-item" href="/HTML/trajets.php">Mes trajets</a>
                    <a class="navbar-item" href="/HTML/proposer_trajet.php">Proposer un trajet</a>
                    <a class="navbar-item" href="/HTML/question.php">Questions</a>

                    <div class="navbar-item has-dropdown is-hoverable">
                        <a class="navbar-link">
                            Plus
                        </a>

                        <div class="navbar-dropdown is-right">

                            <a class="navbar-item" href="./aboutus.php">
                                À Propos
                            </a>
                            <a class="navbar-item" href="../HTML/feedback.php">
                                Signaler un problème
                            </a>
                            <hr class="navbar-divider">
                            <a class="navbar-item" href="../PHP/deconnexion.php">
                                Se déconnecter
                            </a>
                        </div>
                    </div>
                </div>

            </div>
        </nav>
    </header>


    <section class="section">
        <div class="container is-widescreen">
            <h2 class="title is-3 has-text-centered">Bon achat</h2>

            <div class="columns is-mobile is-centered">
                <div class="column is-9">
                    <!--On affiche à l'utilisateur ses informations personnelles -->
                    <div class="box">
                        <?php afficher_bon_achat(getallbon()) ?>
                    </div>
                </div>
            </div>
        </div>
    </section>


</body>

<footer class="footer has-text-centered">
    <p>Copyleft <span class="copyleft">&copy;</span> - VIVIER Tristan : HOUNZANGBE Emmanuel : VANCEL Anthony : BOUTILLIER Kevin</p>
</footer>

</html>