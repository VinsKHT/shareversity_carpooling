<!DOCTYPE html>
<html>


<head>
    <title> Shareversity - CREATION COMPTE</title>
    <meta charset="utf-8" />
    <link rel="stylesheet" href="../CSS/style.css" />
    <script src="/JavaScript/index.js"></script>
</head>

<body>
    <header>
        <nav class="navbar is-black" role="navigation" aria-label="main navigation">
            <!--Logo-->
            <div class="navbar-brand">
                <a class="navbar-item" href="\index.php">
                    <img class="image" src="\IMAGES\logo_shareversity_v2.png" style="max-height: 70px">
                </a>
                <!--Bouton pour la vue du menu sur smartphone-->
                <a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false" data-target="nav-links">
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                </a>
            </div>

            <div id="nav-links" class="navbar-menu">
                <!--Toggle menu actif sur les devices de taille inférieure à 1024px-->
                <div class="navbar-end">
                    <a class="navbar-item" href="/index.php">Accueil</a>
                    <a class="navbar-item" href="/HTML/connexion.html">Connexion</a>
                    <a class="navbar-item" href="/HTML/question.php">Questions</a>

                    <div class="navbar-item has-dropdown is-hoverable">
                        <a class="navbar-link">
                            Plus
                        </a>

                        <div class="navbar-dropdown is-right">

                            <a class="navbar-item" href="./aboutus.php">
                                À Propos
                            </a>
                            <hr class="navbar-divider">
                            <a class="navbar-item" href="/HTML/feedback.php">
                                Signaler un problème
                            </a>
                        </div>
                    </div>
                </div>

            </div>
        </nav>
    </header>


    <section class="section">
        <div class="container is-widescreen">
            <h2 class="title is-3 has-text-centered">Creation de compte</h2>

            <div class="columns is-mobile is-centered">
                <div class="column is-9">
                    <!-- box de Connexion-->
                    <div class="box">
                        <div class="field">
                            <label class="label">Creation de compte</label>
                            <div class="control">
                                <!-- formulaire de creation avec appel de script-->
                                <form action="../PHP/creation_request.php" method="post" autocomplete="on">
                                    <div class="columns">
                                        <div class="column">
                                            <input class="input" type="text" name="Nom" placeholder="Nom" required />
                                        </div>
                                        <div class="column">
                                            <input class="input" type="text" name="Prenom" placeholder="Prenom" required />
                                        </div>
                                    </div>
                                    <div class="columns">
                                        <div class="column">
                                            <input class="input" type="email" name="mail" placeholder="e-mail" required />
                                        </div>
                                        <div class="column">
                                            <input class="input" type="text" name="Pseudo" placeholder="Pseudo" required>
                                        </div>
                                    </div>
                                    <div class="columns">
                                        <div class="column">
                                            <input class="input" type="password" name="mdp" placeholder="Mot de passe">
                                        </div>
                                    </div>
                                    <div class="field">
                                        <div class="control">
                                            <label class="checkbox">
                                                <input type="checkbox" required>
                                                J'accepte les <a href="#">conditions d'utlisation</a>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="columns is-centered">
                                        <input class="button blue_back text_bold" type="submit" value="Créer un compte" />
                                    </div>
                                    <!-- message si champ erroner-->
                                    <?php
                                    if (isset($_SESSION['creation'])) {
                                        $status = $_SESSION['creation'];
                                        echo '<p id="fail">' . $status . '</p>';
                                        unset($_SESSION['creation']);
                                    }
                                    ?>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


</body>

<footer class="footer has-text-centered">
    <p>Copyleft <span class="copyleft">&copy;</span> - VIVIER Tristan : HOUNZANGBE Emmanuel : VANCEL Anthony : BOUTILLIER Kevin</p>
</footer>

</html>