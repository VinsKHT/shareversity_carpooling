<!DOCTYPE html>
<html>
<?php
include '../PHP/services.php';
session_start();
?>

<head>
    <title> Shareversity - FEEDBACK</title>
    <meta charset="utf-8" />
    <link rel="stylesheet" href="../CSS/style.css" />
    <script src="/JavaScript/index.js"></script>
</head>

<body>
    <header>
        <nav class="navbar is-black" role="navigation" aria-label="main navigation">
            <!--Logo-->
            <div class="navbar-brand">
                <a class="navbar-item" href="\index.php">
                    <img class="image" src="\IMAGES\logo_shareversity_v2.png" style="max-height: 70px">
                </a>
                <!--Bouton pour la vue du menu sur smartphone-->
                <a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false" data-target="nav-links">
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                </a>
            </div>
            <div class="navbar-menu" id="nav-links">
                <!--Toggle menu actif sur les devices de taille inférieure à 1024px-->
                <div class="navbar-end">
                <a class="navbar-item" href="/index.php">Accueil</a>
                <?php
                    if (isset($_SESSION['id'])) {
                        echo '<a class="navbar-item" href="/HTML/compte.php">Mon compte</a>';
                        echo '<a class="navbar-item" href="/HTML/trajets.php">Mes trajets</a>';
                        echo '<a class="navbar-item" href="/HTML/bon.php">Bon achat</a>';
                        echo '<a class="navbar-item" href="/HTML/question.php">Questions</a>';
                        echo '<a class="navbar-item" href="/HTML/proposer_trajet.php">Proposer un trajet</a>';
                    } else {
                        echo '<a class="navbar-item" href="/HTML/connexion.html">Connexion</a>';
                        echo '<a class="navbar-item" href="/HTML/creation.php">Création compte</a>';
                        echo '<a class="navbar-item" href="/HTML/question.php">Questions</a>';
                    }
                    ?>
                    <div class="navbar-item has-dropdown is-hoverable">
                        <a class="navbar-link">
                            Plus
                        </a>
                        <div class="navbar-dropdown is-right">

                            <a class="navbar-item" href="./aboutus.php">
                                À Propos
                            </a>
                            <hr class="navbar-divider">
                            <?= isset($_SESSION['id']) ? '<a class="navbar-item" href="/PHP/deconnexion.php">Deconnexion compte</a>' : null ?>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </header>

    <section class="section">
        <div class="container is-widescreen">
            <h2 class="title is-3 has-text-centered"> Signaler un problème</h2>
            <div class="columns is-mobile is-centered">
                <div class="column is-9">
                    <div class="box">
                        <!-- fonctionne pas car mail ne fonctionne pas sur localhost-->
                        <form action="" method="post">
                            <div class="field">
                                <label class="label">Email</label>
                                <div class="control has-icons-left ">
                                    <input class="input" type="email" placeholder="prenom.nom@shareversity.com">
                                    <span class="icon is-small is-left">
                                        <i class="fas fa-envelope"></i>
                                    </span>
                                    <span class="icon is-small is-left"></span>
                                </div>
                            </div>
                            <div class="field">
                                <label class="label">Sujet</label>
                                <div class="control">
                                    <input class="input" type="text" placeholder="How do I....">
                                </div>
                            </div>
                            <div class="field">
                                <label class="label">Ajouter un fichier:</label>
                                <div class="control">
                                    <input class="input" type="file">
                                </div>
                            </div>
                            <div class="field">
                                <label class="label">Message</label>
                                <div class="control">
                                    <textarea class="textarea" placeholder="Votre message..."></textarea>
                                </div>
                            </div>


                            <div class="field is-grouped">
                                <div class="control">
                                    <button class="button blue_back text_bold">Envoyer</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

</body>
<!--Simple form-->
<footer class="footer has-text-centered">
    <p>Copyleft <span class="copyleft">&copy;</span> - VIVIER Tristan : HOUNZANGBE Emmanuel : VANCEL Anthony : BOUTILLIER Kevin</p>
</footer>


</html>