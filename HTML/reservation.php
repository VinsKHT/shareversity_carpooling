<!DOCTYPE html>
<html>
<?php
include '../PHP/services.php';
est_connecte();
?>

<head>
    <title>Shareversity</title>
    <meta charset="utf-8" />
    <link rel="stylesheet" href="../CSS/style.css" />
    <script src="/JavaScript/index.js"></script>
</head>

<body>
    <header>
        <nav class="navbar is-black" role="navigation" aria-label="main navigation">
            <!--Logo-->
            <div class="navbar-brand">
                <a class="navbar-item" href="\index.php">
                    <img class="image" src="\IMAGES\logo_shareversity_v2.png" style="max-height: 70px">
                </a>
                <!--Bouton pour la vue du menu sur smartphone-->
                <a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false" data-target="nav-links">
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                </a>
            </div>
            <div class="navbar-menu" id="nav-links">
                <!--Toggle menu actif sur les devices de taille inférieure à 1024px-->
                <div class="navbar-end">
                    <a class="navbar-item" href="\index.php">
                        Accueil
                    </a>
                    <!--Faire une vérif d'auth et un affchage conséquent de la nav-->
                    <a class="navbar-item" href="\HTML\trajets.php">
                        Mes Trajets
                    </a>
                    <div class="navbar-item has-dropdown is-hoverable">
                        <a class="navbar-link">
                            Plus
                        </a>
                        <div class="navbar-dropdown is-right">
                            <a class="navbar-item" href="./aboutus.php">
                                À Propos
                            </a>
                            <a class="navbar-item" href="/HTML/feedback.php">
                                Signaler un problème
                            </a>
                            <hr class="navbar-divider">
                            <a class="navbar-item" href="/PHP/deconnexion.php">
                                Se déconnecter
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </header>
    <section class="section">
        <div class="container is-widescreen">
            <h2 class="title is-3 has-text-centered">Votre Réservation</h2>
            <div class="columns is-mobile is-centered">
                <div class="column is-10">
                    <div class="box">
                        <div class="control">
                            <?php
                            /* on récupère l'id du trajet qui a été choisi */
                            foreach ($_POST as $name => $reserver) {
                                $id_trajet = $name;
                            }
                            reserver_trajet($id_trajet, $_SESSION['id']);
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</body>


<footer class="footer has-text-centered">
    <p>Copyleft <span class="copyleft">&copy;</span> - VIVIER Tristan : HOUNZANGBE Emmanuel : VANCEL Anthony : BOUTILLIER Kevin</p>
</footer>

</html>