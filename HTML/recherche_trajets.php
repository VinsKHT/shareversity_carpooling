<!DOCTYPE html>
<html>
<?php
include '../PHP/services.php';
session_start();
if (empty($_POST['depart']) || empty($_POST['arrivee'])) {
    header("Location:../index.php");
}
?>
<!--verif sess auth-->

<head>
    <title>RECHERCHE TRAJET</title>
    <meta charset="utf-8" />
    <link rel="stylesheet" href="../CSS/style.css" />
</head>

<body>
    <header>
        <nav class="navbar is-black" role="navigation" aria-label="main navigation">
            <div class="navbar-brand">
                <a class="navbar-item" href="\index.php">
                    <img src="\IMAGES\logo_shareversity_v2.png">
                </a>

                <a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                </a>
            </div>

            <div id="navbarGenerale" class="navbar-menu">
                <!--Toggle menu actif sur les devices de taille inférieure à 1024px-->
                <div class="navbar-end">
                    <a class="navbar-item " href="\index.php">
                        Accueil
                    </a>
                    <?php
                    if (isset($_SESSION['id'])) {
                        echo '<a class="navbar-item" href="/HTML/compte.php">Mon compte</a>';
                        echo '<a class="navbar-item" href="/HTML/trajets.php">Mes trajets</a>';
                        echo '<a class="navbar-item" href="/HTML/question.php">Questions</a>';
                    } else {
                        echo '<a class="navbar-item" href="/HTML/connexion.html">Connexion</a>';
                        echo '<a class="navbar-item" href="/HTML/creation.php">Création compte</a>';
                        echo '<a class="navbar-item" href="/HTML/question.php">Questions</a>';
                    }
                    ?>

                    <div class="navbar-item has-dropdown is-hoverable">
                        <a class="navbar-link">
                            Plus
                        </a>

                        <div class="navbar-dropdown is-right">

                            <a class="navbar-item" href="./aboutus.php">
                                À Propos
                            </a>
                            <a class="navbar-item" href="../HTML/feedback.php">
                                Signaler un problème
                            </a>
                            <hr class="navbar-divider">
                            <?= isset($_SESSION['id']) ? '<a class="navbar-item" href="/PHP/deconnexion.php">Deconnexion compte</a>' : null ?>
                        </div>
                    </div>
                </div>

            </div>
        </nav>
    </header>


    <section class="section">
        <div class="container is-widescreen">
            <h2 class="title is-3 has-text-centered">Resultat recherche trajet</h2>

            <div class="columns is-mobile is-centered">
                <div class="column is-9">
                    <!--On affiche à l'utilisateur ses informations personnelles -->
                    <div class="box">
                        <?php
                        $depart = $_POST['depart'];
                        $arrivee = $_POST['arrivee'];
                        $date = (empty($_POST['date'])) ? '' : $_POST['date'];
                        afficher_resultats_recherche($depart, $arrivee, $date);
                        ?>

                    </div>
                </div>
            </div>
    </section>


</body>

<footer class="footer has-text-centered">
    <p>Copyleft <span class="copyleft">&copy;</span> - VIVIER Tristan : HOUNZANGBE Emmanuel : VANCEL Anthony : BOUTILLIER Kevin</p>
</footer>

</html>