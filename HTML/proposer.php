<!DOCTYPE html>
<html>
<?php
include '../PHP/services.php';
session_start();
if(empty($_SESSION['id'])){
    header("Location:../HTML/connexion.html");
}
if(empty($_POST['date']) || empty($_POST['depart']) || empty($_POST['arrivee']) || empty($_POST['nb_places'])){
    header("Location:/index.php");
}
?>
<!--verif sess auth-->

<head>
    <title>Shareversity</title>
    <meta charset="utf-8" />
    <link rel="stylesheet" href="/CSS/style.css" />
    <script src="/JavaScript/index.js"></script>
</head>

<body>
    <header>
        <nav class="navbar is-black" role="navigation" aria-label="main navigation">
                <!--Logo-->
                <div class="navbar-brand">
                    <a class="navbar-item" href="\index.php">
                        <img class="image" src="\IMAGES\logo_shareversity_v2.png" style="max-height: 70px">
                    </a>
                    <!--Bouton pour la vue du menu sur smartphone-->
                    <a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false" data-target="nav-links">
                        <span aria-hidden="true"></span>
                        <span aria-hidden="true"></span>
                        <span aria-hidden="true"></span>
                    </a>
            </div>

            <div class="navbar-menu" id="nav-links" >
                <!--Toggle menu actif sur les devices de taille inférieure à 1024px-->
                <div class="navbar-end">

                    <?php
                    if (isset($_SESSION['id'])) {
                        echo '<a class="navbar-item" href="/HTML/compte.php">Mon compte</a>';
                        echo '<a class="navbar-item" href="/HTML/trajets.php">Mes trajets</a>';
                        echo '<a class="navbar-item" href="/HTML/question.php">Questions</a>';
                    } else {
                        echo '<a class="navbar-item" href="/HTML/connexion.html">Connexion</a>';
                        echo '<a class="navbar-item" href="/HTML/creation.php">Création compte</a>';
                        echo '<a class="navbar-item" href="/HTML/question.php">Questions</a>';
                    }
                    ?>

                    <div class="navbar-item has-dropdown is-hoverable">
                        <a class="navbar-link">
                            Plus
                        </a>

                        <div class="navbar-dropdown is-right">
                            <a class="navbar-item">
                                À Propos
                            </a>

                            <a class="navbar-item" href="/HTML/aboutus.php">
                                Contact
                            </a>
                            <a class="navbar-item" href="/HTML/feedback.php">
                                Signaler un problème
                            </a>
                            <hr class="navbar-divider">
                            <?= isset($_SESSION['id']) ? '<a class="navbar-item" href="/PHP/deconnexion.php">Deconnexion compte</a>' : null ?>
                            
                            
                        </div>
                    </div>
                </div>

            </div>
        </nav>
    </header>
    <section class="section">
        <div class="container is-widescreen">
            <h2 class="title is-3 has-text-centered">Choix des aires de départ et d'arrivée</h2>

            <div class="columns is-mobile is-centered">
                <div class="column is-9">
                    <div class="box">
                        <div class="field">
                            <label class="label">Aire de départ</label>
                            <div class="control">
                                <form action="../PHP/map.php" method="post" autocomplete="on">
                                    <select name="GPS_depart" id="aire_depart">
                                    <?php
                                        $aires_depart=recuperer_aires($_POST['depart']);
                                        afficher_liste_deroulante_aires($aires_depart);
                                    ?>
                                    </select>
                                    <input class="button blue_back text_bold" type="submit" value="Visualiser l'aire de départ choisie" formtarget="_blank"/>
                                </form>
                            </div>
                        </div>
                        <div class="field">
                            <label class="label">Aire d'arrivée</label>
                            <div class="control">
                                <form action="../PHP/map.php" method="post" autocomplete="on">
                                    <select name="GPS_arrivee" id="aire_arrivee">
                                    <?php
                                        $aires_arrivee=recuperer_aires($_POST['arrivee']);
                                        afficher_liste_deroulante_aires($aires_arrivee);
                                    ?>
                                    </select>
                                    <input class="button blue_back text_bold" type="submit" value="Visualiser l'aire d'arrivée choisie" formtarget="_blank"/>
                                </form>
                                <br><br><br>
                                <div class="columns is-centered">
                                    <form action="./creation_trajet.php" method="post">
                                    <input type="hidden" name="date" value="<?php echo $_POST['date'];?>"/>
                                    <input type="hidden" name="nb_places" value="<?php echo $_POST['nb_places'];?>"/>
                                    <input type="hidden" name="heure_depart" value="<?php echo $_POST['heure_depart'];?>"/>
                                    <script>var selectElmt = document.getElementById("aire_depart");var valeurselectionnee = selectElmt.options[selectElmt.selectedIndex].value;document.write('<input type="hidden" name="aire_depart" value="'+valeurselectionnee+'"/>');</script>
                                    <script>var selectElmt = document.getElementById("aire_arrivee");var valeurselectionnee = selectElmt.options[selectElmt.selectedIndex].value;document.write('<input type="hidden" name="aire_arrivee" value="'+valeurselectionnee+'"/>');</script>
                                    <input class="button blue_back text_bold" type="submit" value="Valider votre choix des aires de départ et d'arrivée" />
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</body>

<footer class="footer has-text-centered">
    <p>Copyleft <span class="copyleft">&copy;</span> - VIVIER Tristan : HOUNZANGBE Emmanuel : VANCEL Anthony : BOUTILLIER Kevin</p>
</footer>

</html>